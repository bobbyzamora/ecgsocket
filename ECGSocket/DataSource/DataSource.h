//
//  DataSource.h
//  ECG
//
//  Created by Misha on 22/07/14.
//  Copyright (c) 2014 Oumobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataSource : NSObject

+(DataSource *)sharedInstance;

@property (nonatomic, strong) NSMutableArray *values;

- (void)startUpdating;

- (void)stopUpdating;

@end
