//
//  DataSource.m
//  ECG
//
//  Created by Misha on 22/07/14.
//  Copyright (c) 2014 Oumobile. All rights reserved.
//

#import "DataSource.h"
#import "Value.h"


@interface DataSource() {
    NSInteger _currentIndex;
    BOOL _stop;
}

@end

@implementation DataSource


#pragma mark - singltone

+(DataSource *)sharedInstance {
    static DataSource *singltone = nil;
    static dispatch_once_t once_t;
    dispatch_once(&once_t, ^
                  {
                      singltone = [[DataSource alloc] init];
                  });
    
    return singltone;
}

#pragma mark - sending source to plot

- (void)startUpdating {
    _stop = NO;
    _values = [[NSMutableArray alloc] init];
}

- (void)stopUpdating {
    _stop = YES;
}



@end
