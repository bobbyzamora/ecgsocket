//
//  ViewController.m
//  ECGSocket
//
//  Created by Misha on 28/01/15.
//  Copyright (c) 2015 Misha. All rights reserved.
//

#import "ViewController.h"
#import "UIView+Helpers.h"
#import "SocketConnector.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate, InputDataDelegate> {
    UIView *_animateView;
    UILabel *_helloLabel;
    
    IBOutlet UITableView *_tableView;
    NSMutableArray *_inputData;
}

@end

@implementation ViewController

#pragma mark - view init

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViews];
    
    _inputData = [[NSMutableArray alloc] init];
    [SocketConnector sharedInstance].inputDataDelegate = self;
    _tableView.alpha = 0.0;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self performHelloStreamAnimation];
}

#pragma mark - views configuration

- (void)configureViews {
    [self createLabel];
    _animateView = [[UIView alloc] initWithFrame:self.view.frame];
    _animateView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_animateView];
}


#pragma mark - table view delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _inputData.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
};
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    
    id input = _inputData[indexPath.row];
    if ([input isKindOfClass:[NSString class]]) {
        cell.textLabel.text = (NSString *)input;
    } else {
        Value *value = (Value*)input;
        cell.textLabel.text = [NSString stringWithFormat:@"%f %f %f %f %f %f and other",value.CL1, value.CL2, value.CL3, value.CL4, value.CL5, value.CL6];
    }
    
    return cell;
};

#pragma mark - inputDataDelegate 

- (void)inputDataString:(NSString *)string {
    [_inputData insertObject:string atIndex:0];
    [_tableView reloadData];
}

- (void)inputValue:(Value *)value {
    [_inputData insertObject:value atIndex:0];
    [_tableView reloadData];
}


#pragma mark - animations

- (void)performHelloStreamAnimation {
    [UIView animateWithDuration:0.8
                     animations:^{
                         _animateView.$x += self.view.$width;
                     } completion:^(BOOL finished) {
                         [self hideHelloLabel];
                     }];
}

- (void)createLabel {
    CGRect bounds = [UIScreen mainScreen].bounds;
    _helloLabel = [[UILabel alloc] initWithFrame:bounds];
    _helloLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:24];
    _helloLabel.textAlignment = NSTextAlignmentCenter;
    _helloLabel.center = self.view.center;
    
    NSAttributedString *string = [[NSAttributedString alloc] initWithString:@"Hello ECG Stream!"];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString:string];
    
    for (int i = 0; i < string.length; i++) {
        [text addAttribute:NSForegroundColorAttributeName
                     value:[UIColor colorWithRed:(arc4random() % 255)/255.0f
                                           green:(arc4random() % 255)/255.0f
                                            blue:(arc4random() % 255)/255.0f alpha:1.0]
                     range:NSMakeRange(i, 1)];
        [_helloLabel setAttributedText: text];
    }
    
    
    
    [self.view addSubview:_helloLabel];
}


- (void)hideHelloLabel {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:1.5 animations:^(void){
            _animateView.$x -= self.view.$width;
        } completion:^(BOOL finished){
            [self animateBackground];
        }];
    });
}

- (void)animateBackground {
    [UIView animateWithDuration:0.5 animations:^(void){
        _tableView.alpha = 1.0;
        [self.view addSubview:_tableView];
        [_tableView reloadData];
    }];
}


@end
