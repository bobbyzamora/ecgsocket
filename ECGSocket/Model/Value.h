//
//  Value.h
//  ECG
//
//  Created by Misha on 17/07/14.
//  Copyright (c) 2014 Oumobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Value : NSObject

@property (nonatomic) float CL1;
@property (nonatomic) float CL2;
@property (nonatomic) float CL3;
@property (nonatomic) float CL4;
@property (nonatomic) float CL5;
@property (nonatomic) float CL6;
@property (nonatomic) float CLL1;
@property (nonatomic) float CLL2;
@property (nonatomic) float CLL3;
@property (nonatomic) float CLL4;
@property (nonatomic) float CLL5;
@property (nonatomic) float CLL6;
@property (nonatomic) int8_t ID;

@end


