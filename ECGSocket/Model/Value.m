//
//  Value.m
//  ECG
//
//  Created by Misha on 17/07/14.
//  Copyright (c) 2014 Oumobile. All rights reserved.
//

#import "Value.h"

@implementation Value

- (NSString *)description {
    return [NSString stringWithFormat:@"Value %d: %.2f, %.2f, %.2f, %.2f, %.2f, %.2f | %.2f, %.2f, %.2f, %.2f, %.2f, %.2f", self.ID, self.CL1, self.CL2, self.CL3, self.CL4, self.CL5, self.CL6, self.CLL1, self.CLL2, self.CLL3, self.CLL4, self.CLL5, self.CLL6];
}

@end

