//
//  AppDelegate.h
//  ECGSocket
//
//  Created by Misha on 28/01/15.
//  Copyright (c) 2015 Misha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

