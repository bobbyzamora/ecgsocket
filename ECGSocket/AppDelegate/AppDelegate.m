//
//  AppDelegate.m
//  ECGSocket
//
//  Created by Misha on 28/01/15.
//  Copyright (c) 2015 Misha. All rights reserved.
//

#import "AppDelegate.h"
#import "SocketConnector.h"
#import "DataSource.h"


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[SocketConnector sharedInstance] connect];
    [[DataSource sharedInstance] startUpdating];
    
    return YES;
}

@end
