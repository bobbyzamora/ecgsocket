//
//  main.m
//  ECGSocket
//
//  Created by Misha on 28/01/15.
//  Copyright (c) 2015 Misha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
