//
//  SocketConnector.h
//  ECGSocket
//
//  Created by Misha on 28/01/15.
//  Copyright (c) 2015 Misha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Value.h"

@protocol InputDataDelegate <NSObject>

- (void)inputDataString:(NSString *)string;
- (void)inputValue:(Value *)value;

@end


@interface SocketConnector : NSObject

@property (nonatomic, assign) id <InputDataDelegate> inputDataDelegate;

+(SocketConnector *) sharedInstance;

- (void)connect;

- (void)disconnect;

- (void)writeSomeBytes:(NSData *)data;

@end
