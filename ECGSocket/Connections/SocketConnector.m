//
//  SocketConnector.m
//  ECGSocket
//
//  Created by Misha on 28/01/15.
//  Copyright (c) 2015 Misha. All rights reserved.
//

#import "SocketConnector.h"
#import "SocketConstants.h"
#import "DataSource.h"
#import <SystemConfiguration/SystemConfiguration.h>

#define STOP_VALUE -32768 //0x8000

@interface SocketConnector() <NSStreamDelegate> {
    Value *_bufferValue;
    NSInteger _currentParseIndex;
}

@property (nonatomic, strong) NSMutableData *writeDataBuffer;
@property (nonatomic, strong) NSInputStream *inputStream;
@property (nonatomic, strong) NSOutputStream *outputStream;

@end


@implementation SocketConnector

#pragma mark - singltone

+(SocketConnector *) sharedInstance {
    
    static SocketConnector *singltone = nil;
    static dispatch_once_t once_t;
    dispatch_once(&once_t, ^
                  {
                      singltone = [[SocketConnector alloc] init];
                  });
    
    return singltone;
}


#pragma mark - connections

- (void)connect {
    
    self.writeDataBuffer = [[NSMutableData alloc] init];
    
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    //get ip&port
    NSString *ip = IP;
    uint port = PORT;
    
    //create tcp connection
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)ip, port, &readStream, &writeStream);
    self.inputStream = (__bridge NSInputStream *)readStream;
    self.outputStream = (__bridge NSOutputStream *)writeStream;
    
    [self.inputStream setDelegate:self];
    [self.outputStream setDelegate:self];
    
    [_inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_inputStream open];
    [_outputStream open];
}

- (void)disconnect {
    [self.inputStream close];
    [self.inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    self.inputStream.delegate = nil;
    self.inputStream = nil;
    
    [self.outputStream close];
    [self.outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    self.outputStream.delegate = nil;
    self.outputStream = nil;
}

#pragma mark - writing

- (void)writeSomeBytes:(NSData *)data {
    [_writeDataBuffer appendData:data];
    [self writeDataFromBufferToStream];
}

- (void)writeDataFromBufferToStream {
    
    while (([_outputStream hasSpaceAvailable]) && ([_writeDataBuffer length] > 0)) {
        
        NSInteger bytesWritten = [_outputStream write:[_writeDataBuffer bytes] maxLength:[_writeDataBuffer length]];
        
        if (bytesWritten == -1) {
            break;
        } else if (bytesWritten > 0)   {
            UInt8 i;
            [_writeDataBuffer getBytes:&i length:1024];
            [_writeDataBuffer replaceBytesInRange:NSMakeRange(0, bytesWritten) withBytes:NULL length:0];
        }
    }
}

#pragma mark - reading

- (void)readDataFromStream {
    NSMutableData *data = [[NSMutableData alloc] init];
    NSLog(@"<< \n Received data:%@ \n length:%d",data,data.length);
    
    [self.inputDataDelegate inputDataString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
    
    uint8_t buf[1024];
    while ([_inputStream hasBytesAvailable]) {
        //get values data
        NSInteger bytesRead = [_inputStream read:buf maxLength:1024];
        [data appendBytes:(const void *)buf length:bytesRead];
    }
    
    if (data.length) {
        [self parse:data];
    }
}

#pragma mark  - parsing

- (void)parse:(NSData *)data {
    /*
     Every x seconds we're receiving 88,75 values, so to prevent losing tail we have _curretParseIndex and _bufferValue value
     So, we parcing received data in order, if in old parse iteration we have some tail, we're parsing new one starting from this tale
     */
    
    
    for (int i = 0; i < (data.length-1); i = i + 2) {
        
        // get value
        int16_t intv;
        [data getBytes:&intv range:NSMakeRange(i, 2)];
        NSLog(@"<< \n Number:%d",intv);
        
        // correspond to values
        switch (_currentParseIndex) {
            case 0:
                _bufferValue = [[Value alloc] init];
                _bufferValue.CL1 = intv;
                break;
            case 1:
                _bufferValue.CL2 = (float)intv;
                break;
            case 2:
                _bufferValue.CL3 = (float)intv;
                break;
            case 3:
                _bufferValue.CLL1 = (float)intv;
                break;
            case 4:
                _bufferValue.CLL2 = (float)intv;
                break;
            case 5:
                _bufferValue.CLL3 = (float)intv;
                break;
            case 6:
                _bufferValue.ID = intv;
                break;
            case 7:
                if (intv == STOP_VALUE) {
                    //configure other values
                    _bufferValue.CL4 = (_bufferValue.CL1 + _bufferValue.CL2) / -2;
                    _bufferValue.CL5 = (_bufferValue.CL1 - _bufferValue.CL3) / 2;
                    _bufferValue.CL6 = (_bufferValue.CL2 + _bufferValue.CL3) / 2;
                    _bufferValue.CLL4 = (_bufferValue.CLL1 + _bufferValue.CLL2) / -2;
                    _bufferValue.CLL5 = (_bufferValue.CLL1 - _bufferValue.CLL3) / 2;
                    _bufferValue.CLL6 = (_bufferValue.CLL2 + _bufferValue.CLL3) / 2;
                    
                    _currentParseIndex = 0;
                    //sending to data source
                    [[DataSource sharedInstance].values addObject:_bufferValue];
                    [self.inputDataDelegate inputValue:_bufferValue];
                    NSLog(@"<< \n Adding value \n FullCount:%d",[DataSource sharedInstance].values.count);
                    continue;
                    
                } else {
                    // some order error occured
                    _currentParseIndex = 0;
                    NSLog(@"Order error! \n this it very bad");
                    continue;
                }
                break;
        }
        
        _currentParseIndex++;
    }
}


#pragma mark - stream delegate

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode {
    switch (eventCode) {
        case NSStreamEventNone:
            
            break;
            
        case NSStreamEventOpenCompleted:
            NSLog(@"<< \n Stream successfully opened");
            break;
            
        case NSStreamEventHasBytesAvailable:
            NSLog(@"<< \n Some data received");
            [self readDataFromStream];
            break;
            
        case NSStreamEventHasSpaceAvailable:
            NSLog(@"<< \n Stream has available space to write");
            [self writeDataFromBufferToStream];
            break;
            
        case NSStreamEventErrorOccurred:
            NSLog(@"<< \n Stream error");
            break;
            
        case NSStreamEventEndEncountered:
            NSLog(@"<< \n Streaming end");
            break;
            
        default:
            break;
    }
}


@end
